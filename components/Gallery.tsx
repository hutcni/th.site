import { useState } from 'react';
import Image from 'next/image';

export type Image = {
	id: number
	name: string
	href: string
	source: string
	listing: string
}

function cn(...classes: string[]) {
	return classes.filter(Boolean).join(' ');
}

export default function Gallery({ images }: { images: Image[] }) {
	return (
		<div className='w-full lg:px-5 pt-5'>
			<div className='grid grid-cols-1 gap-y-6 gap-x-6 lg:grid-cols-3 xl:grid-cols-4'>
				{images?.map(img => <ResponsiveImage key={img.id} image={img} />)}
			</div>
		</div>
	)
}

function ResponsiveImage({ image }: { image: Image}) {
	const [isLoading, setLoading] = useState(true);

	return (
			<a href={image.href} target='_blank' rel="noopener noreferrer" className='group'>
				<div className='aspect-w-1 aspect-h-1 w-full overflow-hidden rounded-lg bg-gray-200 xl:aspect-w-7 xl:aspect-h-8'>
					<Image
							src={image.source}
							layout='fill'
							objectFit='cover'
							className={cn(
									'group-hover:opacity-75 duration-700 ease-in-out',
									isLoading
											? 'grayscale blur-2xl scale-110'
											: 'grayscale-0 blur-0 scale-100'
							)}
							onLoadingComplete={() => setLoading(false)}
					/>
				</div>
			</a>
	)
}