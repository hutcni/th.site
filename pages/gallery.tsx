import { createClient } from '@supabase/supabase-js';
import Gallery, { Image } from '../components/Gallery';


export async function getStaticProps() {
	const supabaseAdmin = createClient(
			process.env.NEXT_PUBLIC_SUPABASE_URL || '',
			process.env.SUPABASE_SERVICE_ROLE_KEY || ''
	)

	const { data } = await supabaseAdmin.from('images').select('*').order('id')

	return {
		props: {
			images: data
		}
	}
}

export default function GalleryPage({ images }: { images: Image[] }) {
	return (
		<Gallery images={images} />
	)
}