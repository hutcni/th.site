import Link from 'next/link';
import Image from 'next/image';
import { NavLink } from '../constants/types';
import { LOGO_SRC, NAVBAR } from '../constants/constants';

const Logo = (): JSX.Element => {
	return (
		<div className="flex px-4 items-center">
			<Link href={'/'}>
				<Image
					src={LOGO_SRC}
					alt={'Logo'}
					height={70}
					width={70}
					className="hover:cursor-pointer hover:opacity-60 duration-300"
				/>
			</Link>
		</div>
	)
}

export const NavBar = () => {
	return (
		<nav className='flex justify-between w-full mx-auto w-full text-black pt-8 pb-4 px-6'>
			<Logo />
			<ul className='flex align-middle'>
				{NAVBAR.map((link: NavLink, index: number) => (
					<li key={index} className='flex px-6 font-sans tracking-widest font-light items-center'>
						<div className='relative nav-item pl-1 hover:text-gray-400 duration-350'>
							<Link href={link.route}>
								{link.text}
							</Link>
						</div>
					</li>
				))}
			</ul>
		</nav>
	)
}