import { NavLink } from './types';

export const NAVBAR: NavLink[] = [
	{
		route: '/gallery',
		text: 'GALLERY'
	},
	{
		route: '/about',
		text: 'ABOUT'
	},
	{
		route: '/contact',
		text: 'CONTACT'
	}
]

export const LOGO_SRC = 'https://i.imgur.com/XOewNgs.jpg';

