export interface NavLink {
	route: string,
	text: string
}